<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230220185336 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE api_clients (id SERIAL NOT NULL, name VARCHAR(100) NOT NULL, token VARCHAR(100) NOT NULL, status SMALLINT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx__api_client__token ON api_clients (token)');
        $this->addSql('CREATE INDEX idx__api_client__status ON api_clients (status)');
        $this->addSql('CREATE UNIQUE INDEX idx__api_client__unique ON api_clients (token)');
        $this->addSql('CREATE TABLE coins (id SERIAL NOT NULL, name VARCHAR(150) NOT NULL, code VARCHAR(50) NOT NULL, type VARCHAR(20) NOT NULL, status SMALLINT NOT NULL, priority SMALLINT DEFAULT 0 NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E818BD577153098 ON coins (code)');
        $this->addSql('CREATE INDEX idx__coins__code ON coins (code)');
        $this->addSql('CREATE INDEX idx__coins__status ON coins (status)');
        $this->addSql('CREATE TABLE rates (date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, base_id INT NOT NULL, quote_id INT NOT NULL, source_id INT NOT NULL, bid NUMERIC(20, 8) NOT NULL, ask NUMERIC(20, 8) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL)');
        $this->addSql('CREATE INDEX idx__rates__base ON rates (base_id)');
        $this->addSql('CREATE INDEX idx__rates__quote ON rates (quote_id)');
        $this->addSql('CREATE INDEX idx__rates__source ON rates (source_id)');
        $this->addSql('CREATE INDEX idx__rates__date ON rates (date)');
        $this->addSql('CREATE UNIQUE INDEX idx__rates__unique ON rates (base_id, quote_id, source_id, date)');
        $this->addSql('CREATE TABLE source (id SERIAL NOT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(50) NOT NULL, type VARCHAR(50) NOT NULL, priority SMALLINT NOT NULL, lifetime INT NOT NULL, executor VARCHAR(500) NOT NULL, executed_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx__source__name ON source (name)');
        $this->addSql('CREATE INDEX idx__source__executor ON source (executor)');
        $this->addSql('CREATE INDEX idx__source__unique ON source (slug, type)');
        $this->addSql('ALTER TABLE rates ADD CONSTRAINT FK_44D4AB3C6967DF41 FOREIGN KEY (base_id) REFERENCES coins (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE rates ADD CONSTRAINT FK_44D4AB3CDB805178 FOREIGN KEY (quote_id) REFERENCES coins (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE rates ADD CONSTRAINT FK_44D4AB3C953C1C61 FOREIGN KEY (source_id) REFERENCES source (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE rates DROP CONSTRAINT FK_44D4AB3C6967DF41');
        $this->addSql('ALTER TABLE rates DROP CONSTRAINT FK_44D4AB3CDB805178');
        $this->addSql('ALTER TABLE rates DROP CONSTRAINT FK_44D4AB3C953C1C61');
        $this->addSql('DROP TABLE api_clients');
        $this->addSql('DROP TABLE coins');
        $this->addSql('DROP TABLE rates');
        $this->addSql('DROP TABLE source');
    }
}
