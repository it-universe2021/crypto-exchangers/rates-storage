<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230220185506 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;');
        $this->addSql("SELECT create_hypertable('rates', 'date', 
            chunk_time_interval => INTERVAL '1 year', create_default_indexes => false,
            if_not_exists => true, migrate_data => true);");
        $this->addSql("SELECT add_dimension('rates', 'base_id', number_partitions => 100);");
        $this->addSql("SELECT add_dimension('rates', 'quote_id', number_partitions => 100);");
        $this->addSql("SELECT add_dimension('rates', 'source_id', number_partitions => 10);");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
    }
}
