<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use DateTime;
use App\Enum\ApiClient\Status AS ApiClientStatus;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230220185403 extends AbstractMigration
{
    private const API_TOKEN = 'iwJ2YmibSRXULONlebYH105sK1ZVPMmwKGc0Vc0lEq7siyxDne';

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $date = new DateTime();
        $this->connection->insert('api_clients', [
            'name' => 'Frontend Application',
            'token' => self::API_TOKEN,
            'status' => ApiClientStatus::ENABLED->value,
            'created_at' => $date->format('Y-m-d H:i:s'),
            'updated_at' => $date->format('Y-m-d H:i:s'),
        ]);

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->connection->delete('api_clients', [
            'token' => self::API_TOKEN
        ]);
    }
}
