<?php
declare(strict_types=1);

namespace App\Type;

class DateTime extends \DateTime implements \Stringable
{

    public function __toString(): string
    {
        return $this->format('Y-m-d H:i:s');
    }
}