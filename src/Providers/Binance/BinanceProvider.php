<?php
declare(strict_types=1);

namespace App\Providers\Binance;

use App\Parsers\JsonResponseParser;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * @property-read ContainerInterface $container
 */
trait BinanceProvider
{
    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function parse(ResponseInterface $response): array
    {
        return (new JsonResponseParser())->parse($response);
    }

    protected function getHost(): string
    {
        return $this->container->getParameter('binance.api.base_url');
    }

    public function getName(): string
    {
        return 'Binance';
    }

    public function getSlug(): string
    {
        return 'binance-' . $this->getType()->value;
    }
}
