<?php
declare(strict_types=1);

namespace App\Providers\Binance;

use App\DTO\Coin;
use App\Providers\CoinsProvider AS BaseCoinsProvider;
use App\Enum\Coin\Type AS CoinType;
use App\Enum\Coin\Status AS CoinStatus;

class CoinsProvider extends BaseCoinsProvider
{
    use BinanceProvider;

    protected function getRequestUrl(): string
    {
        $url = $this->getHost() . $this->getUri();
        $parsed = parse_url($url);
        parse_str($parsed['query'] ?? '', $params);
        $params['timestamp'] = number_format(microtime(true) * 1000, 0, '.', '');
//        $params['recvWindow'] = 50000;
        $query = http_build_query($params, '', '&');
        $query = str_replace([ '%40' ], [ '@' ], $query);
        $signature = hash_hmac('sha256', $query, $this->container->getParameter('binance.api.secret'));

        return $parsed['scheme'] . '://' . $parsed['host'] . $parsed['path'] . '?' . $query . '&signature=' . $signature;
    }

    protected function getHttpClientOptions(): array
    {
        return [
            'headers' => [
                'X-MBX-APIKEY' => $this->container->getParameter('binance.api.key'),
            ],
        ];
    }

    /**
     * @param array $data
     * @return Coin[]
     */
    protected function transform(array $data): array
    {
        return array_map(
            fn(array $coin): Coin => new Coin(
                $coin['name'],
                $coin['coin'],
                match ( $coin['isLegalMoney'] ) {
                    true => CoinType::MONEY,
                    default => CoinType::CRYPTO
                },
                match ( $coin['trading'] ) {
                    true => CoinStatus::ENABLED,
                    default => CoinStatus::DISABLED
                },
                $this->getSource()
            ),
            $data
        );
    }

    protected function getUri(): string
    {
        return $this->container->getParameter('binance.api.endpoint.coins');
    }

    public function getDefaultPriority(): int
    {
        return 250;
    }
}
