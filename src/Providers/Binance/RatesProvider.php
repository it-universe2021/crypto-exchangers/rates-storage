<?php
declare(strict_types=1);

namespace App\Providers\Binance;

use App\DTO\Pair;
use App\DTO\Rate;
use App\Providers\RatesProvider as BaseRatesProvider;
use Exception;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class RatesProvider extends BaseRatesProvider
{
    use BinanceProvider;

    public function getPairsProvider(): PairsProvider
    {
        return $this->container->get('binance.pairs.provider');
    }

    protected function getHttpClientOptions(): array
    {
        return [];
    }

    protected function getUri(): string
    {
        return $this->container->getParameter('binance.api.endpoint.rates');
    }

    /**
     * @throws Exception
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function transform(array $data): array
    {
        $pairs = $this->getPairsProvider()->getData();

        $result = array_map(
            function ($rate) use ($pairs) {
                $filtered = array_filter($pairs, static function (Pair $pair) use ($rate) {
                    return $pair->symbol === $rate['symbol'];
                });
                /** @var Pair $pair */
                $pair = current($filtered);
                if ( ! $pair ) {
                    return null;
                }

                if ( (float)$rate['bidPrice'] === 0.0 && (float)$rate['askPrice'] === 0.0 ) {
                    return null;
                }

                return new Rate(
                    $pair->base,
                    $pair->quote,
                    $this->getSource(),
                    $rate['askPrice'],
                    $rate['bidPrice']
                );
            },
            $data
        );

        return array_filter($result, static function ($item) {
            return $item instanceof Rate;
        });
    }
}
