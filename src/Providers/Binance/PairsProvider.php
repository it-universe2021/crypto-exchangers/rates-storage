<?php
declare(strict_types=1);

namespace App\Providers\Binance;

use App\DTO\Pair;
use App\Providers\Provider;

class PairsProvider extends Provider
{
    use BinanceProvider;

    protected function getHttpClientOptions(): array
    {
        return [];
    }

    /**
     * @param array $data
     * @return Pair[]
     */
    protected function transform(array $data): array
    {
        return array_map(
            static fn($pair): Pair => new Pair(
                $pair['baseAsset'],
                $pair['quoteAsset'],
                $pair['symbol']
            ),
            $data['symbols']
        );
    }

    protected function getUri(): string
    {
        return $this->container->getParameter('binance.api.endpoint.pairs');
    }

}
