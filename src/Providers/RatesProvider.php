<?php
declare(strict_types=1);

namespace App\Providers;

use App\Contracts\RatesProviderInterface;
use App\Enum\Source\Type AS SourceType;

abstract class RatesProvider extends Provider implements RatesProviderInterface
{
    public function getType(): SourceType
    {
        return SourceType::RATES;
    }

    public function getDefaultPriority(): int
    {
        return 500;
    }

    public function getDefaultLifetime(): int
    {
        return 300; //5 min
    }
}
