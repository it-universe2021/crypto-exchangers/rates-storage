<?php
declare(strict_types=1);

namespace App\Providers\Kraken;

use App\DTO\Pair;
use App\Providers\Provider;

class PairsProvider extends Provider
{
    use KrakenProvider;

    /**
     * @param array $data
     * @return Pair[]
     */
    public function transform(array $data): array
    {
        return array_map(
            function ($pair) {
                $assets = explode('/', $pair['wsname']);
                return new Pair(
                    $this->mapCoins($assets[0]),
                    $this->mapCoins($assets[1]),
                    $pair['altname']
                );
            },
            $data
        );
    }

    public function getUri(): string
    {
        return $this->container->getParameter('kraken.api.endpoint.pairs');
    }

}