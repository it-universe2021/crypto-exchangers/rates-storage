<?php

namespace App\Providers\Kraken;

use App\Parsers\JsonResponseParser;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * @property-read ContainerInterface $container
 */
trait KrakenProvider
{
    protected function getHttpClientOptions(): array
    {
        return [
            'headers' => [
                'User-Agent' => 'Kraken PHP API Client',
            ]
        ];
    }

    /**
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function parse(ResponseInterface $response): array
    {
        $result = (new JsonResponseParser())->parse($response);
        return $result['result'];
    }

    protected function mapCoins(string $code): string
    {
        return match ( $code ) {
            'XDG' => 'DOGE',
            'XBT' => 'BTC',
            default => $code
        };
    }

    protected function getHost(): string
    {
        return $this->container->getParameter('kraken.api.base_url');
    }

    public function getName(): string
    {
        return 'Kraken';
    }

    public function getSlug(): string
    {
        return 'kraken-' . $this->getType()->value;
    }
}