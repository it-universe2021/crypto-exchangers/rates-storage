<?php
declare(strict_types=1);

namespace App\Providers\Kraken;

use App\DTO\Pair;
use App\DTO\Rate;
use App\Providers\RatesProvider AS BaseRatesProvider;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Exception;

class RatesProvider extends BaseRatesProvider
{
    use KrakenProvider;

    public function getPairsProvider(): PairsProvider
    {
        return $this->container->get('kraken.pairs.provider');
    }

    public function getUri(): string
    {
        return $this->container->getParameter('kraken.api.endpoint.rates');
    }

    /**
     * @throws Exception
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function transform(array $data): array
    {
        $pairs = $this->getPairsProvider()->getData();

        $result = array_map(
            function ($symbol, $rate) use ($pairs) {
                $filtered = array_filter($pairs, static function (Pair $pair) use ($symbol) {
                    return $pair->symbol === $symbol;
                });
                /** @var Pair $pair */
                $pair = array_shift($filtered);
                if ( ! $pair ) {
                    return null;
                }
                if ( (float)$rate['a'][0] === 0.0  && (float)$rate['b'][0] === 0.0 ) {
                    return null;
                }

                return new Rate(
                    $pair->base,
                    $pair->quote,
                    $this->getSource(),
                    $rate['a'][0],
                    $rate['b'][0]
                );
            },
            array_keys($data), array_values($data)
        );

        return array_filter($result, static function ($item) {
            return $item instanceof Rate;
        });
    }
}