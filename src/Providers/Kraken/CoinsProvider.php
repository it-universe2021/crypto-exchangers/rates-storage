<?php
declare(strict_types=1);

namespace App\Providers\Kraken;

use App\DTO\Coin;
use App\Providers\CoinsProvider AS BaseCoinsProvider;
use App\Enum\Coin\Type AS CoinType;
use App\Enum\Coin\Status AS CoinStatus;

class CoinsProvider extends BaseCoinsProvider
{
    use KrakenProvider;



    /**
     * @param array $data
     * @return Coin[]
     */
    protected function transform(array $data): array
    {
        return array_map(
            fn(string $key, array $coin): Coin => new Coin(
                $key,
                $this->mapCoins($coin['altname']),
                match ( $key ) {
                    'Z' . $coin['altname'] => CoinType::MONEY,
                    default => CoinType::CRYPTO,
                },
                match ( $coin['status'] ) {
                    'enabled' => CoinStatus::ENABLED,
                    default => CoinStatus::DISABLED,
                },
                $this->getSource()
            ),
            array_keys($data), array_values($data)
        );
    }

    protected function getUri(): string
    {
        return $this->container->getParameter('kraken.api.endpoint.coins');
    }

}