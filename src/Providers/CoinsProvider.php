<?php
declare(strict_types=1);

namespace App\Providers;

use App\Contracts\CoinsProviderInterface;
use App\Enum\Source\Type AS SourceType;

abstract class CoinsProvider extends Provider implements CoinsProviderInterface
{
    public function getType(): SourceType
    {
        return SourceType::COINS;
    }

    public function getDefaultPriority(): int
    {
        return 100;
    }

    public function getDefaultLifetime(): int
    {
        return 3600; //2 hours
    }
}
