<?php
declare(strict_types=1);

namespace App\Providers;

use App\Contracts\ProviderInterface;
use App\Entity\Source;
use App\Parsers\JsonResponseParser;
use App\Parsers\NullResponseParser;
use App\Parsers\XmlResponseParser;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;


abstract class Provider implements ProviderInterface
{
    protected ContainerInterface $container;

    protected Source $source;

    public function __construct(private readonly HttpClientInterface $httpClient)
    {
    }

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container): void
    {
        $this->container = $container;
    }

    public function setSource(Source $source): void
    {
        $this->source = $source;
    }

    public function getSource(): Source
    {
        return $this->source;
    }

    /**
     * @return array
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getData(): array
    {
        $data = $this->fetch($this->getRequestUrl());
        $data = $this->parse($data);
        return $this->transform($data);
    }

    /**
     * Get data about coins
     * @throws TransportExceptionInterface
     */
    protected function fetch(string $url): ResponseInterface
    {
        return $this->httpClient->request('GET', $url, $this->getHttpClientOptions());
    }

    /**
     * @param ResponseInterface $response
     * @return array
     * @throws TransportExceptionInterface
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    protected function parse(ResponseInterface $response): array
    {
        $contentType = $response->getHeaders()['content-type'][0];

        $parser = match ($contentType) {
            'application/json', 'application/javascript' => JsonResponseParser::class,
            'text/xml' => XmlResponseParser::class,
            default => NullResponseParser::class,
        };

        return (new $parser())->parse($response);
    }

    protected function getRequestUrl(): string
    {
        return $this->getHost() . $this->getUri();
    }

    public function getExecutor(): string
    {
        return static::class;
    }

    abstract protected function getHttpClientOptions(): array;

    abstract protected function transform(array $data): array;

    abstract protected function getHost(): string;

    abstract protected function getUri(): string;

}
