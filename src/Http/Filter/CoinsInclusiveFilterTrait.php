<?php

namespace App\Http\Filter;

use App\Entity\Coin;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

trait CoinsInclusiveFilterTrait
{
    protected function assertCoinCodes(ExecutionContextInterface $context, array $codes, string $path): array
    {
        $coins = $this->getCoinRepository()->findBy([
            'code' => $codes
        ]);
        if ( count($coins) !== count($codes) ) {
            $unknown = array_diff($codes, array_map(static fn (Coin $coin): string => $coin->getCode(), $coins));
            $context->buildViolation('Unknown currency code(s): ' . implode(', ', $unknown) . '.')
                ->atPath($path)
                ->addViolation();
        }

        return $coins;
    }
}
