<?php
declare(strict_types=1);

namespace App\Http\Filter;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ListCoinsFilter extends AbstractFilter
{
    use PaginatedFilterTrait,
        CoinsInclusiveFilterTrait;

    protected array $coins = [];

    /**
     * @return array
     */
    public function getCoins(): array
    {
        return $this->coins;
    }

    public function setCoins(mixed $coins): void
    {
        if ( is_string($coins) ) {
            $coins = explode(',', $coins);
        }
        $this->coins = array_map(static fn ($coin): string => strtoupper(trim($coin)), $coins);
    }

    #[Assert\Callback]
    public function assertCoins(ExecutionContextInterface $context, $payload): void
    {
        if ( ! empty($this->coins) ) {
            $this->coins = $this->assertCoinCodes($context, $this->coins, 'coins');
        }
    }

}
