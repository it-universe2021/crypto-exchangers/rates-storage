<?php
declare(strict_types=1);

namespace App\Http\Filter;

use App\Entity\Coin;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class LatestRatesFilter extends AbstractFilter
{
    use PaginatedFilterTrait,
        CoinsInclusiveFilterTrait;

    protected array $base = [];

    protected array $quote = [];

    /**
     * @return array
     */
    public function getBase(): array
    {
        return $this->base;
    }

    /**
     * @param mixed $base
     */
    public function setBase(mixed $base): void
    {
        if ( is_string($base) ) {
            $base = explode(',', $base);
        }
        $this->base = array_map(static fn ($code): string => strtoupper(trim($code)), $base);
    }

    /**
     * @return array
     */
    public function getQuote(): array
    {
        return $this->quote;
    }

    public function setQuote(mixed $quote): void
    {
        if ( is_string($quote) ) {
            $quote = explode(',', $quote);
        }
        $this->quote = array_map(static fn ($code): string => strtoupper(trim($code)), $quote);
    }

    #[Assert\Callback]
    public function assertBase(ExecutionContextInterface $context, $payload): void
    {
        if ( ! empty($this->base) ) {
            $this->base = $this->assertCoinCodes($context, $this->base, 'base');
        }
    }

    #[Assert\Callback]
    public function assertQuote(ExecutionContextInterface $context, $payload): void
    {
        if ( ! empty($this->quote) ) {
            $this->quote = $this->assertCoinCodes($context, $this->quote, 'quote');
        }
    }



}
