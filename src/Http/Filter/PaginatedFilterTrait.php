<?php

namespace App\Http\Filter;

use Symfony\Component\Validator\Constraints as Assert;

trait PaginatedFilterTrait
{
    /**
     * @Assert\NotNull()
     * @Assert\Type(type="numeric")
     * @Assert\Positive()
     */
    protected int $page = 1;

    /**
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type(type="numeric")
     * @Assert\Positive()
     */
    protected int $perPage = 10;

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPerPage(): int
    {
        return $this->perPage;
    }

    /**
     * @param int $perPage
     */
    public function setPerPage(int $perPage): void
    {
        $this->perPage = $perPage;
    }


}
