<?php
declare(strict_types=1);

namespace App\Http\Filter;

use App\Contracts\Http\FilterInterface;
use App\Entity\Coin;
use App\Repository\CoinRepository;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

abstract class AbstractFilter implements FilterInterface
{
    use ContainerAwareTrait;

    public function getCoinRepository(): CoinRepository
    {
        return $this->container->get('doctrine')->getRepository(Coin::class);
    }

}
