<?php
declare(strict_types=1);

namespace App\Http\Response;

class SuccessResponse extends AbstractResponse
{
    public function getStatus(): string
    {
        return static::STATUS_SUCCESS;
    }

    public function getStatusCode(): int
    {
        return static::HTTP_OK;
    }

    protected function getDataKey(): string
    {
        return 'data';
    }
}
