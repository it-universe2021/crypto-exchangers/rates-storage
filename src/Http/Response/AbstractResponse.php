<?php
declare(strict_types=1);

namespace App\Http\Response;

use App\Contracts\Http\ResponseInterface;
use App\Http\Context;
use Symfony\Component\HttpFoundation\JsonResponse;
use DateTime;

abstract class AbstractResponse extends JsonResponse implements ResponseInterface
{
    protected ?array $_data = [];
    protected ?array $_meta = [];

    public function __construct(private readonly Context $context, array $data = [], mixed $meta = [], array $headers = [], bool $json = false)
    {
        $this->_data = $data;
        $this->_meta = $meta;

        parent::__construct($data, $this->getStatusCode(), $headers, $json);
    }

    public function update(): static
    {
        $data = [
            'request_id' => $this->getContext()->getRequestId(),
            'status' => $this->getStatus(),
            'timestamp' => (new DateTime())->getTimestamp(),
            $this->getDataKey() => $this->_data,
        ];

        if ( ! empty($this->_meta) ) {
            $data['meta'] = $this->_meta;
        }

        $this->data = json_encode($data, JSON_PRESERVE_ZERO_FRACTION + $this->encodingOptions);

        return parent::update();
    }

    public function getContext(): Context
    {
        return $this->context;
    }



    abstract protected function getDataKey(): string;

}
