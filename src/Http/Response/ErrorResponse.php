<?php
declare(strict_types=1);

namespace App\Http\Response;

class ErrorResponse extends AbstractResponse
{
    public function getStatus(): string
    {
        return static::STATUS_ERROR;
    }

    public function getStatusCode(): int
    {
        return static::HTTP_INTERNAL_SERVER_ERROR;
    }

    protected function getDataKey(): string
    {
        return 'error';
    }
}
