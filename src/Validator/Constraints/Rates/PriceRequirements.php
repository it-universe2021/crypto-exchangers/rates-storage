<?php
declare(strict_types=1);

namespace App\Validator\Constraints\Rates;

use Attribute;
use Symfony\Component\Validator\Constraints\Compound;
use Symfony\Component\Validator\Constraints AS Assert;

#[Attribute]
class PriceRequirements extends Compound
{
    public function getConstraints(array $options): array
    {
        return [
            new Assert\NotBlank(),
            new Assert\NotNull(),
            new Assert\Type('numeric'),
            new Assert\PositiveOrZero(),
        ];
    }
}