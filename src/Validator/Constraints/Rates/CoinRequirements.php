<?php
declare(strict_types=1);


namespace App\Validator\Constraints\Rates;

use App\Entity\Coin;
use Attribute;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Compound;

#[Attribute]
class CoinRequirements extends Compound
{
    protected function getConstraints(array $options): array
    {
        return [
            new Assert\NotNull(),
            new Assert\NotBlank(),
            new Assert\Type(Coin::class),
        ];
    }
}
