<?php
declare(strict_types=1);


namespace App\Validator\Constraints\Rates;

use Attribute;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Compound;

#[Attribute]
class DateRequirements extends Compound
{

    protected function getConstraints(array $options): array
    {
        return [
            new Assert\NotBlank(),
            new Assert\NotNull(),
        ];
    }
}
