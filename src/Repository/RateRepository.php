<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Coin;
use App\Entity\Rate;
use App\Http\Filter\LatestRatesFilter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Rate>
 *
 * @method Rate|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rate|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rate[]    findAll()
 * @method Rate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rate::class);
    }

    public function add(Rate $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Rate $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    private function getLatestRatesExpr(LatestRatesFilter $filter): array
    {
        $query = [];
        $params = [];
        if ( ! empty($filter->getBase()) ) {
            $query[] = 'r.base_id = ANY(:base)';
            $params['base'] = '{' . implode(',', array_map(static fn (Coin $coin): int => $coin->getId(), $filter->getBase())) . '}';
        }
        if ( ! empty($filter->getQuote()) ) {
            $query[] = 'r.quote_id = ANY(:quote)';
            $params['quote'] = '{' . implode(',', array_map(static fn (Coin $coin): int => $coin->getId(), $filter->getQuote())) . '}';
        }

        return [
            !empty($query) ? "WHERE " . implode(' AND ', $query) : '',
            $params,
        ];
    }

    public function getCountLatestRates(LatestRatesFilter $filter): int
    {
        $connection = $this->getEntityManager()->getConnection();
        [$expr, $params] = $this->getLatestRatesExpr($filter);
        $sql = "
            WITH data AS (
                SELECT *, ROW_NUMBER() OVER (PARTITION BY q.base_id, q.quote_id, q.source_id ORDER BY q.bucket DESC) AS rn
                FROM (
                    SELECT
                        time_bucket('1 hour', r.date) AS bucket,
                        r.base_id,
                        r.quote_id,
                        r.source_id,
                        LAST(r.bid, r.date) AS bid,
                        LAST(r.ask, r.date) AS ask,
                        LAST (r.date, r.date) AS date
                    FROM rates AS r
                    {$expr}
                    GROUP BY bucket, r.base_id, r.quote_id, r.source_id
                ) AS q
                WHERE q.bucket >= NOW() - INTERVAL '1 hour'
                ORDER BY q.bucket DESC, q.source_id ASC, q.base_id ASC, q.quote_id ASC 
            )
            SELECT COUNT(*) AS cnt
            FROM data AS d
            WHERE d.rn = 1
        ";

        $stmt = $connection->prepare($sql);


        $resultSet = $stmt->executeQuery($params);
        return $resultSet->fetchAssociative()['cnt'];
    }

    public function getLatestData(LatestRatesFilter $filter, int $offset = 0, int $limit = 5): array
    {
        $connection = $this->getEntityManager()->getConnection();
        [$expr, $params] = $this->getLatestRatesExpr($filter);
        $params['limit'] = $limit;
        $params['offset'] = $offset;

        $sql = "
            WITH data AS (
                SELECT *, ROW_NUMBER() OVER (PARTITION BY q.base_id, q.quote_id, q.source_id ORDER BY q.bucket DESC) AS rn
                FROM (
                    SELECT
                        time_bucket('1 hour', r.date) AS bucket,
                        r.base_id,
                        r.quote_id,
                        r.source_id,
                        LAST(r.bid, r.date) AS bid,
                        LAST(r.ask, r.date) AS ask,
                        LAST (r.date, r.date) AS date
                    FROM rates AS r
                    {$expr}
                    GROUP BY bucket, r.base_id, r.quote_id, r.source_id
                ) AS q
                WHERE q.bucket >= NOW() - INTERVAL '1 hour'
                ORDER BY q.bucket DESC, q.source_id ASC, q.base_id ASC, q.quote_id ASC 
            )
            SELECT *
            FROM data AS d
            WHERE d.rn = 1
            LIMIT :limit OFFSET :offset
        ";

        $stmt = $connection->prepare($sql);
        return $stmt->executeQuery($params)->fetchAllAssociative();

    }

//    /**
//     * @return Rate[] Returns an array of Rate objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('r.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Rate
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
