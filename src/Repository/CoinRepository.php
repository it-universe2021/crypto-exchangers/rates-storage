<?php
declare(strict_types=1);

namespace App\Repository;

use App\Entity\Coin;
use App\Http\Filter\ListCoinsFilter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\QueryException;

/**
 * @extends ServiceEntityRepository<Coin>
 *
 * @method Coin|null find($id, $lockMode = null, $lockVersion = null)
 * @method Coin|null findOneBy(array $criteria, array $orderBy = null)
 * @method Coin[]    findAll()
 * @method Coin[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CoinRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Coin::class);
    }

    public function add(Coin $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return Coin[]
     * @throws QueryException
     */
    public function findAllIndexedByCode(): array
    {
        return $this->createQueryBuilder('c')
            ->indexBy('c', 'c.code')
            ->orderBy('c.code', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Coin[]
     * @throws QueryException
     */
    public function findAllIndexedById(): array
    {
        return $this->createQueryBuilder('c')
            ->indexBy('c', 'c.id')
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function remove(Coin $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getListCoinsCount(ListCoinsFilter $filter): int
    {
        $criteria = [];
        if ( ! empty($filter->getCoins()) ) {
            $criteria['id'] = array_map(static fn(Coin $coin): int => $coin->getId(), $filter->getCoins());
        }

        return $this->count($criteria);
    }

    public function getListCoinsData(ListCoinsFilter $filter, int $offset = 0, int $limit = 10): array
    {
        $criteria = [];
        if ( ! empty($filter->getCoins()) ) {
            $criteria['id'] = array_map(static fn(Coin $coin): int => $coin->getId(), $filter->getCoins());
        }
        return $this->findBy($criteria, ['id' => 'ASC'], $limit, $offset);
    }

//    /**
//     * @return Coin[] Returns an array of Coin objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('a.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Coin
//    {
//        return $this->createQueryBuilder('a')
//            ->andWhere('a.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
