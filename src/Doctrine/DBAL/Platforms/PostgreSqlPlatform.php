<?php
declare(strict_types=1);

namespace App\Doctrine\DBAL\Platforms;

use Doctrine\DBAL\Platforms\PostgreSQLPlatform AS BasePostgreSqlPlatform;
class PostgreSqlPlatform extends BasePostgreSqlPlatform
{

    /**
     * {@inheritDoc}
     */
    public function getListNamespacesSQL()
    {
        return "SELECT schema_name AS nspname
                FROM   information_schema.schemata
                WHERE  schema_name NOT LIKE 'pg\_%'
                AND schema_name NOT LIKE '%timescaledb%'
                AND    schema_name != 'information_schema'";
    }

    public function getListSequencesSQL($database) : string
    {
        return 'SELECT sequence_name AS relname,
                       sequence_schema AS schemaname,
                       minimum_value AS min_value, 
                       increment AS increment_by
                FROM   information_schema.sequences
                WHERE  sequence_catalog = ' . $this->quoteStringLiteral($database) . "
                AND    sequence_schema NOT LIKE 'pg\_%' 
                AND sequence_schema NOT LIKE '%timescaledb\_%'
                AND    sequence_schema != 'information_schema'";
    }

    /**
     * {@inheritDoc}
     */
    public function getListTablesSQL()
    {
        return "SELECT quote_ident(table_name) AS table_name,
                       table_schema AS schema_name
                FROM   information_schema.tables
                WHERE  table_schema NOT LIKE 'pg\_%'
                AND    table_schema NOT LIKE '%timescaledb%'
                AND    table_schema != 'information_schema'
                AND    table_name != 'geometry_columns'
                AND    table_name != 'spatial_ref_sys'
                AND    table_type != 'VIEW'";
    }
}