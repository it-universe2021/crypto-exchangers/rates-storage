<?php
declare(strict_types=1);

namespace App\Security\Api;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;

class AccessDeniedHandler implements AccessDeniedHandlerInterface
{
    public function handle(Request $request, AccessDeniedException $accessDeniedException): ?Response
    {
//        throw $accessDeniedException;
        return new JsonResponse([
            'error' => [
                'code' => $accessDeniedException->getCode(),
                'message' => $accessDeniedException->getMessage(),
            ]
        ], Response::HTTP_UNAUTHORIZED);
    }
}
