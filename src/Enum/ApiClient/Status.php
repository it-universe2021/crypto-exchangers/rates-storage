<?php

namespace App\Enum\ApiClient;

enum Status: int
{
    case ENABLED = 1;
    case DISABLED = 0;
}
