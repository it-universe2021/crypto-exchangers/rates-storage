<?php

namespace App\Enum\Coin;

enum Status : int
{
    case ENABLED = 1;
    case DISABLED = 0;
}
