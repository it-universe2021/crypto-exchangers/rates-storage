<?php
declare(strict_types=1);

namespace App\Enum\Coin;

enum Type: string
{
    case CRYPTO = 'crypto';
    case MONEY = 'money';
}