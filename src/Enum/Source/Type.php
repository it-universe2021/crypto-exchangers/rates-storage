<?php

namespace App\Enum\Source;

enum Type : string
{
    case RATES = 'rates';
    case COINS = 'coins';
}
