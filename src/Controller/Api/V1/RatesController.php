<?php

namespace App\Controller\Api\V1;

use App\Controller\Api\ApiController;
use App\Http\Filter\LatestRatesFilter;
use App\Service\Paginator\RatesPaginator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RatesController extends ApiController
{
    public function __construct(private readonly RatesPaginator $paginator)
    {
    }

    #[Route('/rates/latest', name: 'rates_latest', methods: ['GET'])]
    public function latest(Request $request): JsonResponse
    {
        /** @var LatestRatesFilter $filter */
        $filter = $this->getFilter($request, LatestRatesFilter::class);

        $this->validateFilter($filter);

        $paginator = $this->paginator->getLatestRatesPaginator($filter);

        return $this->response($this->getResult($paginator, ['coin_basic', 'rate_basic', 'source_basic']), $this->getMetaData($paginator));
    }
}
