<?php

namespace App\Controller\Api\V1;

use App\Controller\Api\ApiController;
use App\Http\Filter\ListCoinsFilter;
use App\Service\Paginator\CoinsPaginator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CoinsController extends ApiController
{
    public function __construct(protected readonly CoinsPaginator $paginator)
    {
    }

    #[Route('/coins/list', name: 'coins_list', methods: ['GET'])]
    public function list(Request $request): JsonResponse
    {
        /** @var ListCoinsFilter $filter */
        $filter = $this->getFilter($request, ListCoinsFilter::class);

        $this->validateFilter($filter);

        $paginator = $this->paginator->getListCoinsPaginator($filter);

        return $this->response($this->getResult($paginator, ['coin_basic', 'coin_full']), $this->getMetaData($paginator));
    }
}
