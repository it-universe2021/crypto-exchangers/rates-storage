<?php
declare(strict_types=1);

namespace App\Controller\Api;

use App\Contracts\Http\FilterInterface;
use App\Exceptions\BadDataException;
use App\Http\Response\SuccessResponse;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class ApiController extends AbstractController
{
    protected SerializerInterface $serializer;

    protected ValidatorInterface $validator;

    /**
     * @param SerializerInterface $serializer
     */
    public function setSerializer(SerializerInterface $serializer): void
    {
        $this->serializer = $serializer;
    }

    /**
     * @param ValidatorInterface $validator
     */
    public function setValidator(ValidatorInterface $validator): void
    {
        $this->validator = $validator;
    }

    protected function response(mixed $data, array $meta = [], int $status = Response::HTTP_OK, array $headers = []):JsonResponse
    {
        $response = new SuccessResponse(
            $this->container->get('app.http.context'),
            $data,
            $meta,
            $headers
        );

        $response->setStatusCode($status);

        return $response;
    }

    protected function getFilter(Request $request, string $object): FilterInterface
    {
        /** @var FilterInterface $filter */
        $filter = $this->serializer->denormalize(
            $request->query->all(),
            $object,
            null,
            ['disable_type_enforcement' => true,]
        );
        $filter->setContainer($this->container);

        return $filter;
    }

    protected function validateFilter(FilterInterface $filter): void
    {
        $violations = $this->validator->validate($filter);

        if ( count($violations) > 0 ) {
            throw new BadDataException('Property "' . $violations->get(0)->getPropertyPath() . '": ' . $violations->get(0)->getMessage());
        }
    }

    protected function getResult(Pagerfanta $paginator, array $groups = []): array
    {
        return $this->serializer->normalize($paginator->getCurrentPageResults(), null, [
            DateTimeNormalizer::FORMAT_KEY => 'Y-m-d H:i:s',
            AbstractNormalizer::GROUPS => $groups,
            AbstractObjectNormalizer::SKIP_NULL_VALUES => false,
            AbstractObjectNormalizer::ENABLE_MAX_DEPTH => 2,
        ]);
    }

    protected function getMetaData(Pagerfanta $paginator): array
    {
        return [
            'total' => $paginator->getNbResults(),
            'per_page' => $paginator->getMaxPerPage(),
            'previous_page' => $paginator->hasPreviousPage() ? $paginator->getPreviousPage() : null,
            'current_page' => $paginator->getCurrentPage(),
            'next_page' => $paginator->hasNextPage() ? $paginator->getNextPage() : null,

            'from' => $paginator->getCurrentPageOffsetStart(),
            'to' => $paginator->getCurrentPageOffsetEnd(),
        ];
    }
}
