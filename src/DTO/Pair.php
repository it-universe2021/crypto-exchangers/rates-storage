<?php
declare(strict_types=1);

namespace App\DTO;

class Pair
{
    public function __construct(public readonly string $base, public readonly string $quote, public readonly string $symbol)
    {

    }
}
