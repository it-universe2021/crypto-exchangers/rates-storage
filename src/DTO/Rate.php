<?php
declare(strict_types=1);

namespace App\DTO;

use App\Entity\Source;
use DateTime;
use Exception;
use Litipk\BigNumbers\Decimal;

class Rate
{

    public readonly DateTime $date;

    public readonly string $ask;

    public readonly string $bid;

    /**
     * @throws Exception
     */
    public function __construct(public readonly string $base, public readonly string $quote, public readonly Source $source, string $ask, string $bid, ?string $date = null)
    {
        $this->ask = (string)Decimal::create($ask, 8);
        $this->bid = (string)Decimal::create($bid, 8);
        $this->date = new DateTime($date ?? date('Y-m-d H:i:s'));
    }

}
