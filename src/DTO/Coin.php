<?php
declare(strict_types=1);

namespace App\DTO;

use App\Entity\Source;
use App\Enum\Coin\Type AS CoinType;
use App\Enum\Coin\Status AS CoinStatus;

class Coin
{
    public function __construct(
        public readonly string $name,
        public readonly string $code,
        public readonly CoinType $type,
        public readonly CoinStatus $status,
        public readonly Source $source
    ) {
    }

}
