<?php
declare(strict_types=1);

namespace App\Contracts;

use App\DTO\Coin;

interface CoinsProviderInterface extends SourceProviderInterface
{

    /**
     * @return Coin[]
     */
    public function getData(): array;
}
