<?php

namespace App\Contracts\Http;

interface ResponseInterface
{
    public const STATUS_SUCCESS = 'success';
    public const STATUS_ERROR = 'error';

    public function getStatus(): string;

    public function getStatusCode(): int;

}
