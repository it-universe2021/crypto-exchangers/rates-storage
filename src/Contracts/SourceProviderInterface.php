<?php

namespace App\Contracts;

use App\Enum\Source\Type AS SourceType;

interface SourceProviderInterface extends ProviderInterface
{
    public function getName(): string;

    public function getSlug(): string;

    public function getType(): SourceType;

    public function getExecutor(): string;

    public function getDefaultPriority(): int;

    public function getDefaultLifetime(): int;
}