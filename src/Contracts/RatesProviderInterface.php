<?php
declare(strict_types=1);

namespace App\Contracts;

use App\DTO\Rate;

interface RatesProviderInterface extends SourceProviderInterface
{
    /**
     * @return Rate[]
     */
    public function getData(): array;
}
