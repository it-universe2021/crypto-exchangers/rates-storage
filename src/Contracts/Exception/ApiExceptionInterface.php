<?php

namespace App\Contracts\Exception;

interface ApiExceptionInterface
{
    public const BAD_DATA_EXCEPTION = 1000;

}
