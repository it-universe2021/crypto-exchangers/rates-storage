<?php
declare(strict_types=1);

namespace App\Contracts;

use App\Entity\Source;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

interface ProviderInterface
{
    public function __construct(HttpClientInterface $httpClient);

    public function setContainer(ContainerInterface $container): void;

    public function setSource(Source $source): void;

    public function getData(): array;

}
