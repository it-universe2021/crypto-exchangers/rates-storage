<?php
declare(strict_types=1);

namespace App\DBAL;

use App\Type\DateTime;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class DateTimeType extends \Doctrine\DBAL\Types\DateTimeType
{

    public function convertToPHPValue($value, AbstractPlatform $platform): mixed
    {
        $dateTime = parent::convertToPHPValue($value, $platform);
        if ( ! $dateTime ) {
            return $dateTime;
        }

        $val = new DateTime($dateTime->format('Y-m-d H:i:s'));
        $val->setTimezone($dateTime->getTimezone());

        return $val;
    }

}