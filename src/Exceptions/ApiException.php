<?php
declare(strict_types=1);

namespace App\Exceptions;

use App\Contracts\Exception\ApiExceptionInterface;
use Exception;

class ApiException extends Exception implements ApiExceptionInterface
{

}
