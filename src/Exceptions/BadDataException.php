<?php
declare(strict_types=1);

namespace App\Exceptions;

use Throwable;

class BadDataException extends ApiException
{
    public function __construct(string $message = "", int $code = self::BAD_DATA_EXCEPTION, ?Throwable $previous = null)
    {
        parent::__construct('Bad Data: ' . $message, $code, $previous);
    }
}
