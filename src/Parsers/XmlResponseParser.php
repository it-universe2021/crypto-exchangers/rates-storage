<?php
declare(strict_types=1);


namespace App\Parsers;

use SimpleXMLElement;
use App\Contracts\ResponseParser;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use JsonException;
use Exception;

class XmlResponseParser implements ResponseParser
{

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws JsonException
     * @throws Exception
     */
    public function parse(ResponseInterface $response): array
    {
        $content = $response->getContent();
        return json_decode(
            json_encode(
                new SimpleXMLElement($content),
                JSON_NUMERIC_CHECK | JSON_PRESERVE_ZERO_FRACTION | JSON_THROW_ON_ERROR
            ),
            true,
            512,
            JSON_THROW_ON_ERROR
        );
    }
}
