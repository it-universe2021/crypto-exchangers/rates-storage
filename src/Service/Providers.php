<?php
declare(strict_types=1);

namespace App\Service;

use App\Contracts\ProviderInterface;
use App\Contracts\SourceProviderInterface;
use App\Entity\Source;
use App\Repository\SourceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Providers
{

    private array $sources = [];
    private bool $isProvidersInitialized = false;
    private ContainerInterface $container;

    public function __construct(
        private readonly iterable $dataProviders
    ) {

    }

    public function setContainer(ContainerInterface $container): void
    {
        $this->container = $container;
    }

    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->container->get('doctrine.orm.entity_manager');
    }

    protected function getSourceRepository(): SourceRepository
    {
        return $this->container->get('doctrine')->getRepository(Source::class);
    }

    public function getSource(string $executor): Source
    {
        $this->initProviders();

        if ( isset($this->sources[$executor]) ) {
            return $this->sources[$executor];
        }

        $provider = $this->getProviderByExecutor($executor);

        if ( ! $provider ) {
            throw new \LogicException("Provider '{$executor}' not found.");
        }

        $source = new Source();
        $source->setName($provider->getName());
        $source->setSlug($provider->getSlug());
        $source->setType($provider->getType());
        $source->setPriority($provider->getDefaultPriority());
        $source->setLifetime($provider->getDefaultLifetime());
        $source->setExecutor($executor);
        $this->getSourceRepository()->save($source, true);

        return $source;
    }

    /**
     * @return SourceProviderInterface[]
     */
    public function getDataProviders(): iterable
    {
        return $this->dataProviders;
    }

    public function initProviders(bool $force = false): void
    {
        if ( $this->isProvidersInitialized && ! $force ) {
            return;
        }

        $this->sources = $this->getSourceRepository()->findAllIndexedByExecutor();
        $this->isProvidersInitialized = true;
    }

    private function getProviderByExecutor(string $executor): ?SourceProviderInterface
    {
        foreach ( $this->dataProviders AS $dataProvider ) {
            if ( get_class($dataProvider) === $executor ) {
                return $dataProvider;
            }
        }

        return null;
    }
}