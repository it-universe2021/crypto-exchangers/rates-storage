<?php
declare(strict_types=1);


namespace App\Service;

use App\Entity\Coin;
use App\DTO\Rate AS RateDTO;
use App\Entity\Rate AS RateEntity;
use App\Exceptions\UnknownCoinException;
use App\Type\DateTime;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Validator\Exception\OutOfBoundsException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RatesUpdater
{

    protected array $coins;

    public function __construct(
        private readonly ValidatorInterface $validator,
        private readonly ManagerRegistry $doctrine
    ) {
    }

    public function store(array $rates): void
    {
        $this->coins = $this->doctrine->getManager()->getRepository(Coin::class)->findAllIndexedByCode();

        $em = $this->doctrine->getManager();

        array_walk($rates, function (RateDTO $rateDto) use ( $em ): void {
            try {
                $rate = $this->transform($rateDto);
                $em->persist($rate);
            } catch (UnknownCoinException) {

            }
        }, $rates);

        try {
            $em->flush();
        } catch (UniqueConstraintViolationException) {
            $this->doctrine->resetManager();
        }
    }

    /**
     * @throws UnknownCoinException
     */
    protected function transform(RateDTO $rateDto): RateEntity
    {
        $base = $this->coins[$rateDto->base] ?? null;

        if ( ! $base ) {
            throw new UnknownCoinException("Unknown coin code: '" . $rateDto->base . "', pair: '{$rateDto->base}/{$rateDto->quote}'.");
        }

        $quote = $this->coins[$rateDto->quote] ?? null;

        if ( ! $quote ) {
            throw new UnknownCoinException("Unknown coin code: '{$rateDto->quote}', pair: '{$rateDto->base}/{$rateDto->quote}'.");
        }

        $rate = new RateEntity();
        $rate->setBase($base);
        $rate->setQuote($quote);
        $rate->setBid($rateDto->bid);
        $rate->setAsk($rateDto->ask);
        $rate->setDate(new DateTime($rateDto->date->format('Y-m-d H:i:s')));
        $rate->setSource($rateDto->source);

        $errors = $this->validator->validate($rate);
        if ( count($errors) > 0 ) {
            throw new OutOfBoundsException((string)$errors);
        }

        return $rate;
    }

}
