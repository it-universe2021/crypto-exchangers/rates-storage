<?php
declare(strict_types=1);

namespace App\Service\Paginator;

use App\Entity\Rate;
use App\Http\Filter\LatestRatesFilter;
use App\Repository\CoinRepository;
use App\Repository\RateRepository;
use App\Repository\SourceRepository;
use App\Type\DateTime;
use Pagerfanta\Adapter\CallbackAdapter;
use Pagerfanta\Pagerfanta;

class RatesPaginator
{

    private array $coins = [];
    private array $sources = [];

    public function __construct(
        private readonly RateRepository $repository,
        private readonly CoinRepository $coinRepository,
        private readonly SourceRepository $sourceRepository
    ) {
        $this->coins = $this->coinRepository->findAllIndexedById();
        $this->sources = $this->sourceRepository->findAllIndexedById();

    }

    public function getLatestRatesPaginator(LatestRatesFilter $filter): Pagerfanta
    {
        $adapter = new CallbackAdapter(
            function() use ($filter): int {
                return $this->repository->getCountLatestRates($filter);
            },
            function($offset, $length) use ($filter): array {
                $result = $this->repository->getLatestData($filter, $offset, $length);
                return array_map(function ($item) {
                    $rate = new Rate();
                    $rate->setBase($this->coins[$item['base_id']] ?? null);
                    $rate->setQuote($this->coins[$item['quote_id']] ?? null);
                    $rate->setAsk($item['ask']);
                    $rate->setBid($item['bid']);
                    $rate->setDate(new DateTime($item['date']));
                    $rate->setSource($this->sources[$item['source_id']] ?? null);
                    return $rate;
                }, $result);
            }
        );

        return (new Pagerfanta($adapter))
            ->setCurrentPage($filter->getPage())
            ->setMaxPerPage($filter->getPerPage());
    }
}
