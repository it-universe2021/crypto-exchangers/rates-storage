<?php
declare(strict_types=1);

namespace App\Service\Paginator;

use App\Http\Filter\ListCoinsFilter;
use App\Repository\CoinRepository;
use Pagerfanta\Adapter\CallbackAdapter;
use Pagerfanta\Pagerfanta;

class CoinsPaginator
{

    public function __construct(private readonly CoinRepository $repository)
    {
    }

    public function getListCoinsPaginator(ListCoinsFilter $filter): Pagerfanta
    {
        $adapter = new CallbackAdapter(
            function () use ($filter): int {
                return $this->repository->getListCoinsCount($filter);
            },
            function ($offset, $length) use ($filter): array {
                return $this->repository->getListCoinsData($filter, $offset, $length);
            }
        );

        $paginator = new Pagerfanta($adapter);
        $paginator->setMaxPerPage($filter->getPerPage());
        $paginator->setCurrentPage($filter->getPage());

        return $paginator;
    }

}
