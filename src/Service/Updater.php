<?php
declare(strict_types=1);

namespace App\Service;

use App\Enum\Source\Type AS SourceType;
use Doctrine\Persistence\ManagerRegistry;
use DateTime;

class Updater
{
    public function __construct(
        private readonly Providers $providers,
        private readonly CoinsUpdater $coinsUpdater,
        private readonly RatesUpdater $ratesUpdater,
        private readonly ManagerRegistry $doctrine
    ) {
    }

    public function update(): void
    {
        $now = new DateTime();

        foreach ( $this->providers->getDataProviders() AS $provider ) {
            $source = $this->providers->getSource(get_class($provider));

            if ( $source->getExecutedAt() && $source->getExecutedAt()->getTimestamp() + $source->getLifetime() > $now->getTimestamp() ) {
                continue;
            }
            $provider->setSource($source);

            $data = $provider->getData();

            $updater = match ( $source->getType() ) {
                SourceType::RATES => $this->ratesUpdater,
                SourceType::COINS => $this->coinsUpdater,
                default => null,
            };

            if ( is_null($updater) ) {
                throw new \LogicException("Undefined updater for provider: '{$source->getName()}'.");
            }

            $updater->store($data);

            $source->setExecutedAt(new DateTime());

            $this->doctrine->getManager()->persist($source);
        }
        $this->doctrine->getManager()->flush();
    }
}