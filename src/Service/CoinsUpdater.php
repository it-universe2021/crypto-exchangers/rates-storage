<?php
declare(strict_types=1);


namespace App\Service;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Validator\Exception\OutOfBoundsException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\DTO\Coin AS CoinDto;
use App\Entity\Coin AS CoinEntity;


class CoinsUpdater
{
    protected array $coins;

    public function __construct(private readonly ValidatorInterface $validator, private readonly ManagerRegistry $doctrine)
    {

    }

    public function store(array $coins): void
    {
        $this->coins = $this->doctrine->getRepository(CoinEntity::class)->findAllIndexedByCode();

        $em = $this->doctrine->getManager();

        array_walk($coins, function (CoinDto $coinDto) use ($em): void {

            $entity = $this->coins[$coinDto->code] ?? new CoinEntity();

            $coin = $this->transform($coinDto, $entity);

            if ( ! $coin->getId() ) {
                $em->persist($coin);
            }
        });

        try {
            $em->flush();
        } catch (UniqueConstraintViolationException) {
            $this->doctrine->resetManager();
        }
    }

    protected function transform(CoinDto $coinDto, CoinEntity $coin): CoinEntity
    {
        if ( $coinDto->source->getPriority() >= $coin->getPriority() ) {
            $coin->setName($coinDto->name);
            $coin->setCode($coinDto->code);
            $coin->setType($coinDto->type);
            $coin->setStatus($coinDto->status);
            $coin->setPriority($coinDto->source->getPriority());
        }

        $errors = $this->validator->validate($coin);
        if ( count($errors) > 0 ) {
            throw new OutOfBoundsException((string)$errors);
        }

        return $coin;
    }
}
