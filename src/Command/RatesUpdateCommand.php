<?php

namespace App\Command;

use App\Service\RatesUpdater;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:rates:update',
    description: 'Update rates',
)]
class RatesUpdateCommand extends Command
{
    public function __construct(private readonly RatesUpdater $ratesUpdater)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            ($this->ratesUpdater)();
        } catch (RuntimeException $exception) {
            $io->error('Something went wrong!');
            $io->text($exception->getMessage());
            $io->text($exception->getFile() . ':' . $exception->getLine());

            return Command::FAILURE;
        }

        $io->success('Rates successfully updated.');

        return Command::SUCCESS;
    }
}
