<?php

namespace App\Entity;

use App\Enum\Source\Type AS SourceType;
use App\Repository\SourceRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation AS Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: SourceRepository::class)]
#[ORM\Table(name: 'source')]
#[ORM\Index(fields: ['name'], name: 'idx__source__name')]
#[ORM\Index(fields: ['executor'], name: 'idx__source__executor')]
#[ORM\Index(fields: ['slug', 'type'], name: 'idx__source__unique')]
#[UniqueEntity(fields: ['slug', 'type'])]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', timeAware: false, hardDelete: false)]
class Source
{
    use TimestampableEntity,
        SoftDeleteableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(['source_full', 'source_basic'])]
    private ?string $name = null;

    #[ORM\Column(length: 50)]
    #[Groups(['source_full', 'source_basic'])]
    private ?string $slug = null;

    #[ORM\Column(type: Types::STRING, length: 50, nullable: false, enumType: SourceType::class)]
    #[Groups('source_full')]
    private ?SourceType $type = null;

    #[ORM\Column(type: Types::SMALLINT)]
    private ?int $priority = null;

    #[ORM\Column]
    private ?int $lifetime = null;

    #[ORM\Column(length: 500)]
    private ?string $executor = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $executedAt = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getType(): ?SourceType
    {
        return $this->type;
    }

    public function setType(SourceType $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function getLifetime(): ?int
    {
        return $this->lifetime;
    }

    public function setLifetime(int $lifetime): self
    {
        $this->lifetime = $lifetime;

        return $this;
    }

    public function getExecutor(): ?string
    {
        return $this->executor;
    }

    public function setExecutor(string $executor): self
    {
        $this->executor = $executor;

        return $this;
    }

    public function getExecutedAt(): ?\DateTimeInterface
    {
        return $this->executedAt;
    }

    public function setExecutedAt(?\DateTimeInterface $executedAt): self
    {
        $this->executedAt = $executedAt;

        return $this;
    }
}
