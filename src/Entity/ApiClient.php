<?php

namespace App\Entity;

use App\Repository\ApiClientRepository;
use App\Security\Roles;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation AS Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use App\Enum\ApiClient\Status AS ApiClientStatus;

#[ORM\Entity(repositoryClass: ApiClientRepository::class, readOnly: true)]
#[ORM\Table(name: 'api_clients')]
#[ORM\Index(fields: ['token'], name: 'idx__api_client__token')]
#[ORM\Index(fields: ['status'], name: 'idx__api_client__status')]
#[ORM\UniqueConstraint(name: 'idx__api_client__unique', fields: ['token'])]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', timeAware: false, hardDelete: false)]

class ApiClient implements UserInterface
{
    use TimestampableEntity,
        SoftDeleteableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 100)]
    private ?string $name = null;

    #[ORM\Column(length: 100, unique: true)]
    private ?string $token = null;

    #[ORM\Column(type: Types::SMALLINT, nullable: false, enumType: ApiClientStatus::class)]
    private ?ApiClientStatus $status = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getStatus(): ?ApiClientStatus
    {
        return $this->status;
    }

    public function setStatus(ApiClientStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getUserIdentifier(): string
    {
        return $this->getName();
    }

    public function eraseCredentials(): void
    {

    }

    public function getRoles(): array
    {
        return [
            Roles::ROLE_API_CLIENT,
        ];
    }
}
