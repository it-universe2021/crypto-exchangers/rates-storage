<?php
declare(strict_types=1);

namespace App\Entity;

use App\Enum\Coin\Type AS CoinType;
use App\Enum\Coin\Status AS CoinStatus;
use App\Repository\CoinRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping AS ORM;
use Gedmo\Mapping\Annotation AS Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: CoinRepository::class, readOnly: false)]
#[ORM\Table(name: 'coins')]
#[ORM\Index(fields: ['code'], name: 'idx__coins__code')]
#[ORM\Index(fields: ['status'], name: 'idx__coins__status')]
#[UniqueEntity(fields: ['code'])]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', timeAware: false, hardDelete: false)]


class Coin
{
    use TimestampableEntity,
        SoftDeleteableEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    #[ORM\Column]
    #[Groups(['coin_full'])]
    private ?int $id = null;

    #[ORM\Column(length: 150)]
    #[Groups(['coin_basic'])]
    private ?string $name = null;

    #[ORM\Column(length: 50, unique: true)]
    #[Groups(['coin_basic'])]
    private ?string $code = null;

    #[ORM\Column(type: Types::STRING, length: 20, nullable: false, enumType: CoinType::class)]
    #[Groups(['coin_basic'])]
    private ?CoinType $type = null;

    #[ORM\Column(type: Types::SMALLINT, enumType: CoinStatus::class)]
    #[Groups(['coin_full'])]
    private ?CoinStatus $status = null;

    #[ORM\Column(type: Types::SMALLINT, nullable: false, options: ['default' => 0])]
    private int $priority = 0;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getType(): ?CoinType
    {
        return $this->type;
    }

    public function setType(?CoinType $type): void
    {
        $this->type = $type;
    }

    public function getStatus(): ?CoinStatus
    {
        return $this->status;
    }

    public function setStatus(CoinStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPriority(): int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): void
    {
        $this->priority = $priority;
    }

}
