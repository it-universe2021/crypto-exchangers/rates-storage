<?php
declare(strict_types=1);

namespace App\Entity;

use App\Repository\RateRepository;
use App\Validator\Constraints\Rates as Assert;
use App\Type\DateTime;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: RateRepository::class, readOnly: true)]
#[ORM\Table(name: 'rates')]
#[ORM\Index(fields: ['base'], name: 'idx__rates__base')]
#[ORM\Index(fields: ['quote'], name: 'idx__rates__quote')]
#[ORM\Index(fields: ['source'], name: 'idx__rates__source')]
#[ORM\Index(fields: ['date'], name: 'idx__rates__date')]
//#[ORM\UniqueConstraint(name: 'idx__rates__unique', fields: ['base', 'quote', 'source', 'date'])]
#[Gedmo\SoftDeleteable(fieldName: 'deletedAt', timeAware: false, hardDelete: false)]

class Rate
{
    use TimestampableEntity,
        SoftDeleteableEntity;

//    #[ORM\Id]
//    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
//    #[ORM\Column]
//    private ?int $id = null;

    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Coin::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\CoinRequirements]
    #[Groups(['rate_basic'])]
    private ?Coin $base = null;

    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Coin::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Assert\CoinRequirements]
    #[Groups(['rate_basic'])]
    private ?Coin $quote = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 20, scale: 8)]
    #[Assert\PriceRequirements]
    #[Groups(['rate_basic'])]
    private ?string $bid = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 20, scale: 8)]
    #[Assert\PriceRequirements]
    #[Groups(['rate_basic'])]
    private ?string $ask = null;

    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Source::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['rate_basic'])]
    private ?Source $source = null;

    #[ORM\Id]
    #[ORM\Column]
    #[Assert\DateRequirements]
    #[Groups(['rate_basic'])]
    private ?DateTime $date = null;

//    public function getId(): ?int
//    {
//        return $this->id;
//    }

    public function getBase(): ?Coin
    {
        return $this->base;
    }

    public function setBase(?Coin $base): self
    {
        $this->base = $base;

        return $this;
    }

    public function getQuote(): ?Coin
    {
        return $this->quote;
    }

    public function setQuote(?Coin $quote): self
    {
        $this->quote = $quote;

        return $this;
    }

    public function getBid(): ?string
    {
        return $this->bid;
    }

    public function setBid(?string $bid): self
    {
        $this->bid = $bid;

        return $this;
    }

    public function getAsk(): ?string
    {
        return $this->ask;
    }

    public function setAsk(?string $ask): self
    {
        $this->ask = $ask;

        return $this;
    }

    public function getSource(): ?Source
    {
        return $this->source;
    }

    public function setSource(?Source $source): void
    {
        $this->source = $source;
    }

    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }
}
