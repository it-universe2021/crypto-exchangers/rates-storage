<?php
declare(strict_types=1);

namespace App\EventSubscriber;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\Uid\Uuid;

abstract class AbstractSubscriber implements ContainerAwareInterface, EventSubscriberInterface
{
    use ContainerAwareTrait;
}
