<?php
declare(strict_types=1);

namespace App\EventSubscriber;

use App\Http\Response\ErrorResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class ExceptionSubscriber extends AbstractSubscriber
{
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => [
                ['process', 10],
                ['log', 5],
            ],
        ];
    }

    public function process(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        $error = [
            'code' => $exception->getCode(),
            'message' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
        ];

        $response = new ErrorResponse($this->container->get('app.http.context'), $error);
        $statusCode = match ( true ) {
            $exception instanceof AuthenticationException => Response::HTTP_UNAUTHORIZED,
            $exception instanceof AccessDeniedException => Response::HTTP_FORBIDDEN,
            $exception instanceof HttpExceptionInterface => $exception->getStatusCode(),
            default => $response->getStatusCode()
        };

        $response->setStatusCode($statusCode);

        $event->setResponse($response);
    }

    public function log(ExceptionEvent $event): void
    {

    }

}
