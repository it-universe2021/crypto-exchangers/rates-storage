<?php
declare(strict_types=1);

namespace App\EventSubscriber;

use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Uid\Uuid;

class ResponseSubscriber extends AbstractSubscriber
{

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::RESPONSE => [
                ['process', 10]
            ],
        ];
    }

    public function process(ResponseEvent $event): void
    {
    }

}