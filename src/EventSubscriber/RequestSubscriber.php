<?php
declare(strict_types=1);

namespace App\EventSubscriber;

use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Uid\Uuid;

class RequestSubscriber extends AbstractSubscriber
{

    protected string $header = 'X-Request-Id';

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => [
                ['process', 500]
            ],
        ];
    }

    public function process(RequestEvent $event): void
    {
        $request = $event->getRequest();
        $id = $request->headers->has($this->header) ? $request->headers->get($this->header) : Uuid::v4()->toRfc4122();

        $this->container->get('app.http.context')->setRequestId($id);
    }
}
